(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))
(ql:quickload "fiasco")
(ql:quickload "coalton")
(ql:quickload "str")

(defpackage #:my-package
  (:use 
   #:coalton 
   #:coalton-prelude))
   
(in-package #:my-package)

(named-readtables:in-readtable coalton:coalton)

(coalton-toplevel
  ;; Variables are defined with the define keyword
  (define x 5)
  (define y 6)
  (define z (+ x y))
  (define p (Tuple 1.0 2.0))

  ;; Coalton supports integers, strings, booleans, and unit as primitive types
  (define name "Alyssa P. Hacker")
  (define hungry True)
  (define data Unit))
  
  
