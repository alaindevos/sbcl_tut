(load "~/quicklisp/setup.lisp")

(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))

(ql:quickload "serapeum")
(ql:quickload "fiasco")
(ql:quickload "coalton")

(defpackage mymain
  (:use #:cl
        #:serapeum)
  (:export main));defpackage

(defpackage mycoalton
  (:use #:coalton 
        #:coalton-prelude)
  (:local-nicknames (#:mycl #:cl))
  (:export cmain) );defpackage

(in-package :mycoalton)

(coalton-toplevel
    (declare ctext String)
    (define  ctext "Hello World")

    (declare myprint ( String -> Unit ))
    (define (myprint s)
        (Lisp Unit (s) (mycl:princ s) Unit)
        );define

    (declare cmain ( Unit -> Unit ))
    (define (cmain) (myprint ctext))
    );toplevel

(in-package :mymain)
(-> main () Integer )
(defun main ()
	(mycoalton:cmain coalton:Unit)
	0 
	)
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
