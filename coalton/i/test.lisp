
(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))
(ql:quickload "fiasco")
(ql:quickload "coalton")
(ql:quickload "postmodern")

(defpackage mytest
  (:use #:coalton #:coalton-prelude)
  (:export cmain ) );defpackage

(in-package :mytest)

(cl:defun mycreatetable ()
  ;(postmodern:query (:create-table 'persons ((name :type cl:string) (age :type cl:integer))))
  );defun

(cl:defun myinsert ()
  (postmodern:query  (:insert-into 'persons :set :name "Alain" :age 20 ));query
  (postmodern:query  (:insert-into 'persons :set :name "Eddy"  :age 30)));defun

(cl:defun myquery ()
    (cl:let 
        ((myname nil)
         (myage  nil))
        (cl:princ (postmodern:doquery (:select 'name 'age :from 'persons) (myname myage) (cl:print myname) (cl:print myage)))
        ))

(cl:defun myconnect ()
    (cl:princ "connect")
    (postmodern:connect-toplevel "x" "x" "x" "127.0.0.1" :port 5432 ))

(cl:defun myprint (s)
    (cl:princ s)
    )

(coalton-toplevel
    
    (declare myprintwrapper ( Unit -> Unit ))
    (define (myprintwrapper z)
       (lisp Unit (z) (myprint (myquery)) Unit)
       );define


    (declare myquerywrapper ( String -> Unit ))
    (define (myquerywrapper y)
       (
       myprintwrapper (lisp Unit (y) (myquery) Unit))
       )
        
    (declare mywrapper ( String -> Unit ))
    (define (mywrapper x)
        (lisp Unit (x) (myconnect)(mycreatetable)(myinsert)Unit)
        (myquerywrapper x)       
        );define

    (declare cmain ( Unit -> Unit ))
    (define (cmain) (mywrapper "") ) );toplevel

(in-package :cl)

(defun main ()
	(mytest:cmain coalton:Unit ) )
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
