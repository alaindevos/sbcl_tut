
(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))
(ql:quickload "fiasco")
(ql:quickload "coalton")

(defpackage mytest
  (:use #:coalton #:coalton-prelude)
  (:export cmain callouterprint)
  (:local-nicknames (#:cell #:coalton-library/cell)))

(in-package :mytest)
(cl:defun innerprint (s) (cl:print s))
(cl:defun returnhello () "Hello1" )
(cl:defun printhi ()(cl:print "Hi"))

(coalton-toplevel
    (define-type Foo (Foo Integer String))
    (define-type FooList (FooList (List Foo)) )
    ; empty FooList
	(define e (FooList Nil))
	; prepend x to the list of FooList f
	(define (pre x f)
	(match f
		((FooList l) (FooList (cons x l)))))
   	(define-type IntList
		(MyCons Integer IntList)
		 MyNil);define
    (declare mypush (Integer -> IntList -> IntList))
    (define (mypush x y)
        MyCons x y)
    (declare mypop (IntList  -> Integer))
    (define (mypop x)
        (match x
            ((MyCons y z )  y )
            ((MyNil      )  0 )))
    (define atest (MyCons 1 (MyCons 2 (MyCons 3 MyNil))))
    (declare sumlist ( IntList  -> Integer))
    (define (sumlist x)
        (match x
            ((MyCons y z) (+ y (sumlist z)))
            ((MyNil)      0   )))
    (define anint 2)
    (define  ctext "Hello World")
    (define retval (cell:new "Initial"))
    (declare addtwo ( Integer  ->  Integer  ))
    (define (addtwo x)  ( + x anint) )
    (declare outerprintstring ( String -> Unit ))
    (define (outerprintstring s)
        (lisp Unit (s) (innerprint s) Unit));---------------------------
    (declare outerprintint ( Integer -> Unit ))
    (define (outerprintint s)
        (lisp Unit (s) (innerprint s) Unit))
    (declare cmain ( Unit -> Unit ))
    (define (cmain) (outerprintstring ctext)
                       (outerprintint (sumlist atest))
                       (outerprintstring "Hello2")
                       (outerprintstring (lisp  String () (returnhello)))  ;--------------------
                       (cell:write! retval "Modified")
                       (lisp  Unit() (printhi) Unit)
                       Unit
                       )
                       );coalton


(in-package :cl)
(defun main ()
	(mytest:cmain coalton:Unit ) )

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
