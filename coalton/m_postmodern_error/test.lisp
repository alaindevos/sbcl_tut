(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))
(ql:quickload "serapeum")
(ql:quickload "fiasco")
(ql:quickload "coalton")
(ql:quickload "postmodern")
(defpackage package-main
    (:use   #:cl
            #:serapeum)
    (:local-nicknames 
           (#:n #:coalton))
    (:export main)) ;defpackage
(defpackage package-coalton
    (:use   #:coalton 
            #:coalton-prelude)
    (:local-nicknames
            (#:n #:coalton)
            (#:l #:cl) 
            (#:pm #:postmodern))
    (:export cmain) ) ;defpackage
(in-package :package-coalton)
(coalton-toplevel
    (declare myconnect ( Unit -> Unit ))
    (define (myconnect)
        (Lisp Unit () (pm:connect-toplevel "x" "x" "x" "127.0.0.1" :port 5432 ) Unit))
    (declare myquery ( Unit -> String ))
    (define (myquery)
        (Lisp String ()
            (l:let ((myname nil)
                    (myage  nil)
                    (mysum  nil))
                (pm:doquery 
                    (:select 'name 'age :from 'persons)
                    (myname myage)
                    (l:setf mysum 
                        (l:concatenate l::'string mysum (l:format nil "~a:~a ~%" myname myage)))) ;doquery
                mysum)))
    (define (mystring)
        "TESTF")
;   (declare cmain ( Unit -> Unit ))
    (define (cmain)
        ; (myconnect)
        (Lisp Unit () (l:print "TestC") Unit) 
        (Lisp Unit () (l:format l:t "TestD") Unit) 
        (Lisp Unit () (l:format l:t "TestE ~a" mystring) Unit) 
        ;(Lisp Unit () (l:format l:t "Test2:~a ~%" (myquery Unit)) Unit) 
        Unit)) ;toplevel
(in-package :package-main)
(-> main () Integer )
(defun main ()
    (print "TESTA")
	(package-coalton:cmain n:Unit) 
    (print "TESTB")
    
    0 ) ; main
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
;-------------------------------------------------------------------------------------------------------------
