(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))
(ql:quickload "fiasco")
(ql:quickload "coalton")

(defpackage mytest
  (:use #:coalton #:coalton-prelude)
  (:export cmain ) );defpackage

(in-package :mytest)
(cl:defun innerprint (s)
  (cl:princ s))

(coalton-toplevel
    (declare ctext String)
    (define  ctext "Hello World")

    (declare outerprint ( String -> Unit ))
    (define (outerprint s)
        (lisp Unit (s) (innerprint s) Unit)
        );define

    (declare cmain ( Unit -> Unit ))
    (define (cmain) (outerprint ctext) )
    );toplevel

(in-package :cl)
(defun main ()
	(mytest:cmain coalton:Unit ) )
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
