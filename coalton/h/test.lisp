(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))
(ql:quickload "fiasco")
(ql:quickload "coalton")
(defpackage pack
  (:use :coalton :coalton-prelude)
  (:export main L sumList) ) ;defpackage
( in-package :pack)
(named-readtables:in-readtable coalton:coalton)

(coalton-toplevel
    (define-type IntList
        (MyCons Integer IntList) MyNil) ;define

    (declare L IntList)
    (define L (MyCons 1 (MyCons 2 (MyCons 3 MyNil))))

    (declare sumlist ( IntList  -> Integer))
    (define (sumlist x)
        (match x
            ((MyCons y z) (+ y (sumlist z)))
            ((MyNil)      0   ))) ;define

    (declare printInt ( Integer -> Unit ))
    (define (printInt s)
        (lisp Unit (s) 
            (cl:princ s) 
            Unit)
        ) ;define

    (declare main ( Unit -> Unit))
    (define (main) (printInt (sumlist L))) ;define
    ) ;toplevel


(in-package :cl)
(defun main ()
    (princ (pack:sumlist pack:L))
    (pack:main coalton:Unit ) ) ;defun
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
 
