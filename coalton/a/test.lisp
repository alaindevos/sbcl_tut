(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))
(ql:quickload "fiasco")
(ql:quickload "coalton")

(defpackage mytest
  (:use #:coalton #:coalton-prelude)
  (:export cmain) );defpackage

(in-package :mytest)
(named-readtables:in-readtable coalton:coalton)

(coalton-toplevel
    (define anint 2)
    (declare addtwo ( Integer  ->  Integer  ))
    (define (addtwo x)  ( + x anint) )
    (declare cmain ( Unit -> Integer ))
    (define (cmain ) (addtwo 3)) );toplevel


(in-package :cl)
(defun main ()
	( print (mytest:cmain coalton::Unit) ) );main
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
