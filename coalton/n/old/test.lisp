(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))
(ql:quickload "serapeum")
(ql:quickload "coalton")

(defpackage package-coalton
    (:use 
        #:coalton 
        #:coalton-prelude)
    (:local-nicknames 
        (#:l #:cl))
    (:export cmain) );defpackage

(defpackage package-main
    (:use
        #:cl 
        #:serapeum)
    (:local-nicknames 
        (#:n #:coalton))
    (:export main)) ;defpackage

(in-package :package-coalton)
(coalton-toplevel
    (declare cmain ( Unit -> Unit ))
    (define (cmain)
        (Lisp Unit () 
            (l:princ "Hello World\n") 
            Unit))) ;toplevel

(in-package :package-main)
( -> main () Integer )
(defun main ()
    (package-coalton:cmain n:Unit) 
    0) ; main
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
