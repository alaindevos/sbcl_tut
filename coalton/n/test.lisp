(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))
(ql:quickload "serapeum")
(ql:quickload "fiasco")
(ql:quickload "coalton")
(ql:quickload "postmodern")

(defpackage mytest
  (:use #:coalton #:coalton-prelude)
  (:local-nicknames
            (#:n #:coalton)
            (#:l #:cl) 
            (#:pm #:postmodern))
  (:export cmain ) );defpackage


(in-package :mytest)
(cl:defun innerprint (s)
  (cl:princ s))

(coalton-toplevel
    (declare ctext String)
    (define  ctext "Hello World")
    (declare outerprint ( String -> Unit ))
    (define (outerprint s)
        (lisp Unit (s) (innerprint s) Unit)
        );define
    (declare myconnect ( Unit -> Unit ))
    (define (myconnect)
    (Lisp Unit () (pm:connect-toplevel "x" "x" "x" "127.0.0.1" :port 5432 ) Unit))
    (declare myquery ( Unit -> String ))
    (define (myquery)
        (Lisp String ()
            (l:let ((myname nil)
                    (myage  nil)
                    (mysum  nil))
                (pm:doquery 
                    (:select 'name 'age :from 'persons)
                    (myname myage)
                    (l:setf mysum 
                        (l:concatenate l::'string mysum (l:format nil "~a:~a ~%" myname myage)))) ;doquery
                mysum)))
    (declare cmain ( Unit -> Unit ))
    (define (cmain) 
        (myconnect)
        (myquery Unit)
        Unit
        )
    );toplevel

(in-package :cl)
(defun main ()
    ;(postmodern:connect-toplevel "syslogng" "x" "x" "127.0.0.1" :port 5432 )
	(mytest:cmain coalton:Unit ) )
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
