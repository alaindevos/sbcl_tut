(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))
(ql:quickload "serapeum")
(ql:quickload "fiasco")
(ql:quickload "coalton")
(ql:quickload "postmodern")

(defpackage mymain
  (:use #:cl
        #:serapeum)
  (:export main)) ;defpackage

(defpackage mycoalton
  (:use #:coalton 
        #:coalton-prelude)
  (:local-nicknames (#:co #:cl) (#:pm #:postmodern))
  (:export cmain) ) ;defpackage

(in-package :mycoalton)
(coalton-toplevel
    (declare myconnect ( Unit -> Unit ))
    (define (myconnect)
        (Lisp Unit()
            (pm:connect-toplevel "x" "x" "x" "127.0.0.1" :port 5432 ) 
            Unit))
    (declare myquery ( Unit -> String ))
    (define (myquery)
        (Lisp String()
            (co:let ((myname nil)
                       (myage  nil)
                       (mysum  nil))
                (pm:doquery (:select 'name 'age :from 'persons)
                    (myname myage)
                    (co:setf mysum (co:concatenate co::'string mysum (co:format nil "~a:~a ~%" myname myage))))
                mysum)))
    (declare cmain ( Unit -> Unit ))
    (define (cmain)
        (myconnect)
        (Lisp Unit()
            (co:format co:t "~a ~%" (myquery Unit))
            Unit) 
        Unit)) ;toplevel

(in-package :mymain)
(-> main () Integer )
(defun main ()
	(mycoalton:cmain coalton:Unit)
	0 ) ; main
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
