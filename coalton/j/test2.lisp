(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

(ql:quickload :postmodern)
(use-package :postmodern)


(defun mycreatetable ()
  (query (:create-table 'persons
          ((name :type string)
           (age :type integer)))));defun

(defun myinsert ()
  (query (:insert-into 'persons :set :name "Alain" :age 20 ));query
  (query (:insert-into 'persons :set :name "Eddy"  :age 30)));defun

(defun myquery ()
  (let ((myname nil)
        (myage  nil))
    (doquery (:select 'name 'age :from 'persons)
             (myname myage)
             (print myname)
             (print myage)
             )));defun

(defun main ()
  (connect-toplevel "x" "x" "x" "127.0.0.1" :port 5432 )
  (myinsert)
  (myquery));main

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
