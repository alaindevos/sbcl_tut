(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))
(ql:quickload "fiasco")
(ql:quickload "coalton")

(defpackage mytest
  (:use #:coalton #:coalton-prelude)
  (:export cmain ) );defpackage

(in-package :mytest)

(coalton-toplevel
    (define  ctext "Hello World")

    (declare clprint ( String -> Unit ))
    (define (clprint s)
        (lisp Unit (s) (cl:print s) Unit)
      );defun

    (declare cmain ( Unit -> Unit ))
    (define (cmain) (clprint ctext) ));toplevel


(in-package :cl)
(defun main ()
	(mytest:cmain coalton:Unit ) )

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
