(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))
(ql:quickload "fiasco")
(ql:quickload "coalton")

(defpackage mytest
  (:use #:coalton #:coalton-prelude)
  (:export cmain) );defpackage

( in-package :mytest )

(named-readtables:in-readtable coalton:coalton)


(coalton-toplevel
	(define-type IntList
		(MyCons Integer IntList)
		 MyNil);define

    (declare mypush (Integer -> IntList -> IntList))
    (define (mypush x y)
        MyCons x y)

    (declare mypop (IntList  -> Integer))
    (define (mypop x)
        (match x
            ((MyCons y z )  y )
            ((MyNil      )  0 )));define

    (define atest (MyCons 1 (MyCons 2 (MyCons 3 MyNil))))

      (declare sumlist ( IntList  -> Integer))
    (define (sumlist x)
        (match x
            ((MyCons y z) (+ y (sumlist z)))
            ((MyNil)      0   )));define
    (define (cmain) (sumlist atest)));toplevel


(in-package :cl)
(defun main ()
	(print (mytest:cmain coalton::Unit)) );main

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
