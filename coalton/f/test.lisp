(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))
(ql:quickload "fiasco")
(ql:quickload "coalton")

(defpackage pack
  (:use #:coalton #:coalton-prelude)
  (:export cmain) ) ;defpackage

( in-package :pack )

(named-readtables:in-readtable coalton:coalton)

(coalton-toplevel
    (define-type IntList
        (MyCons Integer IntList) MyNil) ;define

    (declare L IntList)
    (define L (MyCons 1 (MyCons 2 (MyCons 3 MyNil))))

    (declare sumlist ( IntList  -> Integer))
    (define (sumlist x)
        (match x
            ((MyCons y z) (+ y (sumlist z)))
            ((MyNil)      0   ))) ;define

    (declare cmain ( Unit -> Integer))
    (define (cmain) (sumlist L))) ;toplevel


(in-package :cl)
(defun main ()
    (print (pack:cmain coalton::Unit)) ) ;main

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
