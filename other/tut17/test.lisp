(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))


(defun return35 ()
  (values 3 5)
  )

(defun main ()
  (let  ((x nil)
         (a 0)
         (b 0))
    (setf x (cons "Alain" "DeVos"))
    (print (type-of  x) )
    (destructuring-bind (a . b) x
        (print a)
        (setf x (cons "Alain" '()))
        (print (type-of x))
        (setf x (list 1 2 3))
        (print  (type-of x))
      );destruct
    "Association list"
    (setf x (list (cons 'Alain 40) (cons  'Eddy 50)))
    (print  (assoc 'Alain x))
    (setf (cdr (assoc 'Alain x))  70)
    (push (cons 'Jan 80) x)
    "Property  list, indexed  by  symbols"
    (setf x (list 'Alain 40 'Eddy 50))
    (print (getf x 'Alain) )
    (setf (getf x 'Alain) 60)
    (multiple-value-bind (a b) (return35)
      (print a)
      )
    );let
  );defun

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
