(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))
(ql:quickload "defstar")

(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload "defstar"))

(defpackage package-library
    (:use #:cl #:defstar)
    (:export printstring))

(in-package :package-library)
(defun* (printstring -> boolean) ((s string))
    (princ s) t)

;(sb-ext:save-lisp-and-die "library.so" )
