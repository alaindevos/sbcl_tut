(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))

;(sb-alien:load-shared-object "library.so")
(load "library.lisp")

(in-package :cl)
(defun main()
	(package-library:printstring "Hello World"))
(sb-ext:save-lisp-and-die "main.exe" :toplevel #'main :executable t)
