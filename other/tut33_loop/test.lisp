(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))
(defun main ()
    (let ((tel 0))
        (loop
            (cond 
                ((equal tel 5) (return))
                (T (setf tel (+ 1 tel))(print tel)))))
    0)
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
