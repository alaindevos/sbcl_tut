(declaim (optimize (speed 3) (safety 3)))
(load "~/quicklisp/setup.lisp")


(let ((c 0))
    (defun my-counter! ()
        (lambda ()
            (setf c (+ 1 c))
	    c); lambda
	 ) ;defun
) ;let

(defun main ()
(print (funcall (my-counter!)))
(print (funcall (my-counter!)))
)


(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
