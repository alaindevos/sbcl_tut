(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

(ql:quickload "series")
(ql:quickload "folio2")
(use-package :series )
(series::install :macro t :shadow t)

(defun fibs (&optional (x 0) (y 1))
  (declare (optimizable-series-function 1))
  (catenate (scan (list x y))                      ; initial 2 values
            (collecting-fn '(values integer integer)   ; Rest of the values
                           (lambda () (values y x))     ; Initial values
                           (lambda (y x)                ; Inductive Step
                             (values (+ y x) y)))))

(defun main ()
  (print (folio2:take  10 (fibs))))

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
