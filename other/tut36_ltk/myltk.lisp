(load "~/quicklisp/setup.lisp")
(ql:quickload "ltk")
(in-package :ltk-user)
(declaim (optimize (speed 3) (safety 3)))
(defun main()
  (with-ltk ()
   (let ((b (make-instance 'button
                           :master nil
                           :text "Press Me"
                           :command (lambda ()
                                      (format t "Hello World!~&")))))
     (pack b))))
(main)
