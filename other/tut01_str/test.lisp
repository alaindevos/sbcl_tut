(load "~/quicklisp/setup.lisp")

(declaim (optimize (speed 3) (safety 3)))

(ql:quickload :str)

(defconstant *mynul* 0)
(defparameter *myint* 1)

(defun  mysetval ()
  (let ((x 1))
     (setf x 2)
     (print x)));defun


(declaim (ftype (function (string string) null ) myfunction))
(defun myfunction (x y)
  (print (str:concat x " " y))
  nil);defun

(declaim (ftype (function (list) null)  tprintlist))
(defun tprintlist (x)
  (print x)
  nil);defun


(defun printlist ()
	(let  ( (A '(1 2 3))
	        (B (list 1 2 3))
            (C (cons '1 (cons '2 (cons '3 '() ) )  ))
          )
		(tprintlist A)
		(tprintlist B)
        (tprintlist C)));defun

(defun myif ()
  (if ( < 0 1 )
      (print "YES")
      (print "NO")
    ))

(defun mycompare (x y)
    (print "start")
    (cond ( ( equal x y ) (print "equal")
            (  < x y    ) (print "smaller")
            (  > x y    ) (print "bigger")
              T           (print "other")
                   )
          );cond
    (print "stop"));defun

(defun myvar ()
  (let ( (x 2) )
    (setf x 3)
    (print x)));defun

;;; my function comment
(defun mydoc ()
  "This is my function documentation string"
  ;
  )

(defun mylist ()
  (let ((alist '(1 2 3)))
    (append '(1 2 ) '(3 4 ))
    (cons '1 '(2 3))
    (reverse '(1 2 3 ))
    (nthcdr 1 '(1 2 3))
    (nth 1 '(1 2 3))
    (print  (length '(1 2 3)))
    (print (member 2 '(1 2 3)))
    (push 0 alist)
    (print (pop alist))
    (subst 3 5 '(1 2 3 4))
    ));defun

(defun myset ()
  (member '1 '(1 2 ))
  (intersection '(1 2) '(2 3))
  (union '(1 2) '(2 3))
  (set-difference '(1 2) '(2 3 )));defun

(defun myhash ()
  (let ((name_age '(
                    (Alain 20)
                    (Eddy 30)
                    );hash
                  ));assign
    (print (cdr (assoc 'Alain name_age) ))));defun

(defun  myarray ()
  (let ((ar (make-array 10)))
    (setf (aref ar 0) 15)
    (print (aref ar 0))));defun

(defun mymapcar ()
  ( let ((li '(1 2 3)))
    (print
      (mapcar
        (lambda (x) (+ 1 x));lambda
        li)));let

  (mapcar (lambda (n) (* n n ))
          '(1 2 3 4 )));defun


(defun mypredicate ()
  (print (atom 1))
  (print (listp  '(1 2)))
  (print (null  nil))
  (print (equal 1 1))
  (print (numberp 6))
  (print (member 1 '(1  2 3))));defun

(defstruct (scar)
    name
    year);defstruct

(defun mystruct ()
  (let ((acar ( make-scar)))
  (print  "A" )
  (setf (scar-name acar ) "Opel" )
  (print  "B" )
  (setf (scar-year acar ) 1999 )
  (print "C" )
  (print (scar-name acar))
  (print "D" )
  (print (scar-year acar))
  (print "E")));defun

(defun mykeyword (&key x y)
  (print (- x y)));defun

(defun mydo (x y)
  (do ((result 1)
       (exponent y))
      ((zerop exponent) result) ; termination condition <-> return value
    (setf result (* x result))
    (setf exponent (- exponent 1))));defun

(defun myloop1 ()
  (let ((tel 0)
        (sum 0)
        (prod 1))
       (loop (setf tel (+ 1 tel))
             (when ( > tel 5 ) (return prod))
             (setf prod (* prod tel)))));defun

(defun myloop2 ()
  (loop for i from 1 to 5 do
        (print i)));defun

(defun myloop3 ()
  (loop for x in '(1 2 3) do
        (print x)));defun

(defun mydotimes ()
  (dotimes (i 4)
    (print i)));defun

(defun mydolist ()
  (let ((x '(123)))
    (dolist (y x) (print y))));defun

(defun mydotimes ()
  (dotimes (i 4) (print i)));defun

(defun mydatatypes ()
  (let ((achar #\c)
        (astring "Alain")
        (bstring "Alain")
        (asymbol 'Alain))
    (print achar)
    (print astring)
    (print (string-equal astring bstring))
    (print asymbol)));defun

(defclass aperson () (
   ( name :type string  :reader rname :accessor wname :initarg :iname )
   ( age  :type integer :reader rage  :accessor wage  :initarg :iage )));defclass

(defmethod getname ((ap aperson))
  (rname ap))

(defmethod setname ((ap aperson) newname)
  (setf (wname ap) newname))

(defun myclass ()
  (let ((me (MAKE-INSTANCE 'aperson :iname "Alain" :iage 20 )))
    (setf (wname me) "Eddy")
    (print (rname me))
    (setname me "Jos")
    (print (getname me))));defun


(defun myeval ()
      (print (eval `(- 2 3))));defun

(defun mymin (x y)
  (- x y));defun

(defun myfuncall ( x y)
  (funcall 'mymin x y))

(defun makeadd (delta)
  (lambda (x) (+ x delta)));defun


(defun mylambda ()
  (let ((add2 (makeadd 2)))
    (print (funcall add2 3))));defun

(defun myarray2dim ()
  (let ((a nil))
    (setf a #2A ((0 1 2 )
                 (3 4 5 )
                 (6 7 8 )
                 )
          );setf
    (setf (aref a 1 1 ) 42)
    (print (aref a 1 1))));defun


(defun myhash2 ()
  (let ((h nil))
    (setf h (make-hash-table :test 'equal ))
    (setf (gethash "1" h ) 20 )
    (setf (gethash "2" h ) 30 )
    (print (gethash "1" h ))));defun

(defun myrecurse ()
  (let ((lis '(1 2 3 ) ))
        (defun mycount (x)
          (cond ( (null x) 0)
                ( T        ( + 1 (mycount (rest x))))))
        (print (mycount lis))));defun

(defun myerror ()
  (if (equal 1 0 )
      (error "this is an error")
      (print "ok")));defun

(defun mystring ()
  (let ((astring "Alain")
        (achar   nil))
    (setf achar (aref astring 2))
    (print achar)));defun

(defun main ()
  (mysetval)
  (myfunction "Alain" "De Vos")
  (printlist)
  (myif)
  (mycompare 0 1)
  (myvar)
  (mydoc)
  (mylist)
  (myset)
  (myhash)
  (myarray)
  (mymapcar)
  (mypredicate)
  (mystruct)
  (mykeyword :x 2 :y 3 )
  (print (mydo 2 3))
  (print (myloop1))
  (myloop2)
  (myloop3)
  (mydotimes)
  (mydolist)
  (mydotimes)
  (mydatatypes)
  (myclass)
  (myeval)
  (print (myfuncall 2 3))
  (mylambda)
  (myarray2dim)
  (myhash2)
  (myrecurse)
  (myerror)
  (mystring))

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
