(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))

(defun main()
    (flet
        ((myprint (x y) (format t "~a:~a~%" x y)))
        (funcall #'myprint "Hello" "World")
        (apply #'myprint '("Hello" "World"))
        (myprint "Hello" "World")
    
))


(sb-ext:save-lisp-and-die "main.exe" :toplevel #'main :executable t)
