(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))
(ql:quickload :postmodern)
(use-package :postmodern)

(defun myquery ()
  (let ( (firstname)  (lastname) (sum) );let
    (doquery (:select 'person.firstname 'person.lastname :from 'person)
                     ( firstname lastname)
                     (setf sum (concatenate 'string firstname "|" lastname))
             (print sum)
             )));defun

(defun main ()
  (connect-toplevel "x" "x" "x" "127.0.0.1" :port 5432 )
  (myquery));main

(sb-ext:save-lisp-and-die "h.exe" :toplevel #'main :executable t)
