#|
Comment 
|#

(terpri) ;newline
(print nil) ;false
(print t) ;true
(let ((x 1)
      (y 2)
      z)
    (print x))

(let* ((x 3)
       (y x))
    (print y))

(let (x)
    (setq x 4)
    (print x))

;;square
(defun f (x)
    (print x)
    (* x x))
(print (f 3))

;;quote == don't evaluate
(print (quote x))
(print 'x)
(print (quote (0 1 2 3))) ; do not call the function 0
(print '(0 1 2 3))

;;symbol == enum in C
(defun do-command (c)
    (if (equal c 'print )
        (print "Hi")
        nil
        );if
    );defun
(do-command 'print)
(do-command 'read)

(defun sum (lst)
    (if (null lst)
        0
        ( + (car lst) (sum (cdr lst))) ;+
        ) ;if
    ) ;defun
(print (sum '(1 2 3 )))


(defun m-len (lst)
    (if (null lst)
        0
        ( + 1 (m-len (cdr lst))) ;+
        ) ;if
    ) ;defun
(print (m-len '(1 2 3 )))

(defun print-list (lst)
    (if (null lst)
        nil
        (progn  ; block
            (print (car lst))
            (print-list (cdr lst))
            )
        ) ;if
    ) ;defun
(print-list '(1 2 3 ))


(defun m-sum (lst)
    (cond
        ((null lst) 0)
        ((numberp (car lst)) 
            (+ (car lst) (sum (cdr lst)))
            ) ;numberp
        (t nil)
        ) ;cond
    ) ;defun
(trace m-sum)
(print (m-sum '(1 2 3 )))
(print (m-sum '('1 '2 '3 )))

;; test if subset
(defun m-subset (l1 l2)
    (cond
        ((null l1 ) t)
        (t
            (and
                (member (car l1) l2)
                (m-subset (cdr l1) l2));and
                )
        ) ;cond
    ) ;defun 
(print (m-subset '(1 2) '(1 2 3)))
(print (m-subset '(1 4) '(1 2 3)))

;;remove all occurences of an atom in a list

(defun m-delete (a lst)
    (cond
         ((null lst) '())
         ((equal a (car lst)) (m-delete a (cdr lst)))
         (t (cons (car lst) (m-delete a (cdr lst))))
        ) ;cond
    );defun
(print (m-delete 2 '(1 2 3 4 2 6)))

(defun m-max (lst)
    (if(null lst)
        0
        (if (> (car lst) (m-max (cdr lst)))
            (car lst)
            (m-max (cdr lst))
            ) ;if 
        ) ;if 
    ) ;defun 
(print (m-max '(1 2 5 3 4)))