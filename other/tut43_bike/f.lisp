(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))
(ql:quickload :uiop)
(sb-vm::set-floating-point-modes :traps (remove :invalid (getf (sb-vm::get-floating-point-modes) :traps)))
(ql:quickload :bike)

(defpackage doit
    (:use :cl)
    (:use :bike)
    (:export hello)
    )
    
(in-package :doit)
(defun hello () 
    (named-readtables:in-readtable bike-syntax)
    (import-assembly 'System.Runtime.InteropServices.RuntimeInformation)
    (use-namespace 'System)
    (use-namespace 'System.Runtime.InteropServices)
    (let ((myinput))
        (setf myinput "Hello")
        (setf myinput (invoke 'System.Console 'ReadLine))
        (invoke 'System.Console 'WriteLine myinput)
        );let
    );defun 
    
(setf uiop:*image-entry-point* #'doit::hello)
(uiop:dump-image "f.exe" :executable t)
