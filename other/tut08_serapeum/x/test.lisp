(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

(ql:quickload "serapeum")

(declaim (type (integer) *anint*))
(defparameter *anint* 2)

(-> addtwo ( integer ) integer )
(defun addtwo (x)
  ( + *anint* x ))

(defun main ()
	(print (addtwo 3)));main

(defun main ()
  (alain::main))

(main)
