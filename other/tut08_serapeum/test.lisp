(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

(ql:quickload "serapeum")

(defpackage alain
  (:use :cl :serapeum)
  (:export main));defpackage

(in-package :alain)

(declaim (type (integer) *anint*))
(defparameter *anint* 2)

(-> addtwo ( integer ) integer )
(defun addtwo (x)
  ( + *anint* x ))

(defun main ()
	(print (addtwo 3)));main

(in-package :cl)
(defun main ()
  (alain::main))
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
