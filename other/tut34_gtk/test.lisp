(load "~/quicklisp/setup.lisp")
(ql:quickload :cl-cffi-gtk)

(defpackage :mypak
      (:use :gtk :common-lisp))
(in-package :mypak)

; Main window

(defun mymain ()
    (let ((window (make-instance 'gtk:gtk-window :type :toplevel :title "Bleep"))
          (vbox (make-instance 'gtk:gtk-box :orientation :vertical :spacing 25 :margin 25))
         )
        (gtk:within-main-loop
            (gobject:g-signal-connect window "destroy" 
                (lambda (widget) 
                    (declare (ignore widget))
            (gtk:leave-gtk-main)))
            ; Display GUI
            (gtk:gtk-container-add window vbox)
            (gtk:gtk-widget-show-all window)))
            (gtk:join-gtk-main))
  

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'mypak::mymain :executable t)
