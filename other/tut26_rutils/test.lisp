(load "~/quicklisp/setup.lisp")
(ql:quickload :rutils)
(in-package :rtl-user)
(defun main()
    (let
        ((x nil))
      (format T "~A" "Hallo")
      (:= x (vec 1 2 3))
      (write (elt x 2))))

(sb-ext:save-lisp-and-die "test.exe" :toplevel 'main :executable t)
