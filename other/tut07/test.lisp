(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

(declaim (type (integer) *anint*))
(defparameter *anint* 2)

(declaim (ftype (function (integer) integer) addtwo))
(defun addtwo (x)
  ( + *anint* x ))


(defun main ()
	(print (addtwo 3)));main

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
