(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))
(ql:quickload "serapeum")
(ql:quickload "coalton")

(defpackage package-main
    (:use
        #:cl 
        #:serapeum)
    (:local-nicknames 
        (#:n #:coalton))
    (:export main printstring)) ;defpackage

(defpackage package-coalton
    (:use 
        #:coalton-prelude
        #:coalton)
    (:local-nicknames 
        (#:l  #:cl)
        (#:nl #:coalton-library/list)
        (#:ns #:coalton-library/string)
        (#:nc #:coalton-library/cell)
                )
    (:export cmain));defpackage

;-----------------------------------------------------------------------
(in-package :package-coalton)
(coalton-toplevel

    (declare txtmod (cell String))
    (define txtmod (nc:new " Variable-text "))

    (define-type IntList (IntList (List Integer)))
    (declare alist IntList)
    (define alist (IntList(range 1 5)))
    (declare blist IntList)
    (define blist (IntList Nil))

    (declare joinstring(String -> String -> String))
    (define(joinstring x y)
        (ns:concat x y))

    (declare getstring(Unit -> String))
    (define (getstring)
        (ns:concat astring bstring))

    (declare astring String)
    (define astring " Hello ")
    (declare bstring String)
    (define bstring " World ")

    (declare printstring (String -> Unit))
    (define (printstring s)
        (Lisp Unit(s) (l:princ s) Unit))

    (declare printlist(IntList -> Unit))
    (define (printlist s)
        (Lisp Unit(s) (l:princ s) Unit))

    (declare cmain (Unit -> Unit))
    (define (cmain)
        (nc:write! txtmod " New Text ")
        (Lisp Unit() (package-main:printstring (nc:read txtmod)) Unit)
        (printlist alist)
        (printstring(getstring)) Unit)) ;toplevel

;-----------------------------------------------------------------------
(in-package :package-main)
( -> printstring (String) Boolean )
(defun printstring (s)
    (princ s) t)
( -> main () Integer)
(defun main()
    (package-coalton:cmain n:Unit) 0) ; main
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
