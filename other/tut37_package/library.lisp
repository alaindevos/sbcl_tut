(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))
(ql:quickload "serapeum")

(defpackage package-library
    (:use
        #:cl 
        #:serapeum)
    (:export main printstring))

(in-package :package-library)
(defun printstring (s)
    (princ s) t)
    
