(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))
(ql:quickload "serapeum")

(load "library.lisp")

(in-package :cl)

(defun main()
	(package-library::printstring "Hello World"))
	
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
