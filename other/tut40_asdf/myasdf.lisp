(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))

(load "~/quicklisp/setup.lisp")
(ql:quickload "alexandria")

(format t " ~a ~% "(asdf:already-loaded-systems))
(asdf:load-systems :mypack)
(format t " ~a ~% "(asdf:already-loaded-systems))
(mypack:printme "Hello World")
