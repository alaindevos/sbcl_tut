(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))
(in-package :mypack)
(defun* (printme -> boolean) ((s string))
    (princ s) t)
