(defsystem "mypack"
    :author "Alain De Vos <devosalain@ymail.com>"
    :description "My test"
    :version "0.0.1"
    :license "BSD-3-Clause"
    :depends-on (:alexandria :uiop :defstar)
    :components 
        ((:module "src"
          :components
          ((:file "packages")
           (:file "main")))))
          
    
