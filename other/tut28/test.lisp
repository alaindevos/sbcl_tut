(load "~/quicklisp/setup.lisp")

(declaim (optimize (speed 3) (safety 3)))

(let ((limit 3)
      (counter -1))
    (defun my-counter ()
      (if (< counter limit)
          (incf counter)
          (setf counter 0))))


(defun main ()
	(print (mapcar #'(lambda (n)(* n n)) '(1 2 3 4 5)))
	(print (my-counter))
	(print (my-counter))
	(print (my-counter))
	(print (my-counter))
	(print (my-counter))
)

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
