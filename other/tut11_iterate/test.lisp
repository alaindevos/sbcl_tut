(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

(ql:quickload "iterate")
(use-package :iterate)


 (defvar fib
     (let ((x 0)
            (y 1)
            (xbis nil))
        (lambda ()
                 (setf xbis x)
                 (setf x y)
                 (setf y (+ xbis y))
                 x
          )));defvar

(defun doit ()
  (print
    (iter
      (for i from 1 to 10)
        (collect
          (funcall fib)
          ))));defun

(defun main ()
  (doit));defun

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
