(load "~/quicklisp/setup.lisp")
;(compile-file "./ltk/ltk")
;(load "./ltk/ltk")
(ql:quickload "ltk")
(in-package :ltk)

(declaim (optimize (speed 3) (safety 3)))

(defun main()
  (with-ltk ()
   (let ((b (make-instance 'button
                           :master nil
                           :text "Press Me"
                           :command (lambda ()
                                      (format t "Hello World!~&")))))
     (pack b))))

(sb-ext:save-lisp-and-die "myltk.exe" :toplevel #'main :executable t)
