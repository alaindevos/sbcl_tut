(load "~/quicklisp/setup.lisp")
(ql:quickload :alexandria)



(defun adder (foo bar)
  "Add the two arguments."
  (+ foo bar))

(defvar add-one (alexandria:curry #'adder 1) "Add 1 to the argument.")

(defun main()
    (progn
        (print (funcall add-one 10))  ;; => 11
        (setf (symbol-function 'add-one) add-one)
        (print (add-one 10))  ;; => 11
        (write (alexandria:first-elt '(1 2 3)))
        )
    ) ;defun

(sb-ext:save-lisp-and-die "test.exe" :toplevel 'main :executable t)
