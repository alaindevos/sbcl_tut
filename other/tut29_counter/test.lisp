(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))

(ql:quickload "serapeum")

(defpackage mypack
  (:use :cl :serapeum)
  (:export main));defpackage

(in-package :mypack)

(let ((c 0))
    (-> my-counter! () (-> () integer) )
    (defun my-counter! ()
       "COUNTER"
        #'(lambda ()
            (setf c (+ 1 c))
	    c))) ;let

(defun main ()
    (print (funcall (my-counter!)))
    (print (funcall (my-counter!))))


(in-package :cl)
(defun main ()
  (mypack::main))
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
