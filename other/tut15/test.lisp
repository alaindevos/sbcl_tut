(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

(defun main ()
  (let ((tel  0))
    (loop
      (setf tel (+ 1 tel))
      (if (> tel 4) (return)
                    nil
          )
      (print tel)
      );loop
    );let
  );defun

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
