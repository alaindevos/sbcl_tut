(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3) (space 0) (debug 3)))
(defun main ()
    (print 
        (apply  #'cons '(a ()) ))
    (print
        (funcall #'cons 'a () ))
    (print
        (eval '(cons 'a ()) ))
    0)
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
