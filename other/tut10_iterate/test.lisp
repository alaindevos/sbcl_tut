(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

(ql:quickload "iterate")
(use-package :iterate)

(defun myiter1 ()
    (iter (for i from 1 to 5)
        (print i)));defun

(defun myiter2 ()
    (iter (for x in '(1 2 3 4 5))
        (print x)));defun

(defun main ()
  (myiter1)
  (myiter2));defun

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
