(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

(defpackage alain
  (:use :cl)
  (:export main)
  );defpackage

(in-package :alain)

(defun main ()
	(print "Hello World"));main

(in-package :cl)
(defun main ()
  (alain::main))

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
