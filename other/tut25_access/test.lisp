(load "~/quicklisp/setup.lisp")
(ql:quickload :access)
(defun main()
    (let
        ((x nil))
      (format T "~A" "Hallo")
      "Property  list, indexed  by  symbols"
      (setf x (list 'Alain 40 'Eddy 50))
      (format T "~A" (access:plist-val 'Alain x))
      ))

(sb-ext:save-lisp-and-die "test.exe" :toplevel 'main :executable t)
