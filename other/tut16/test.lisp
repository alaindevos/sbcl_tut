(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

(defstruct (person (:type vector) :named) name  age )
(defstruct person2 (name2 "Alain") (age2 20))

(defun main ()
  (let  ((x nil))
    (setf x (make-person :name "Alain" :age 20))
    (print (person-name x))
    (print (person-age  x))
    (print (type-of  (person-name x)))
    (print (type-of  (person-age  x)))
    );let
  );defun

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
