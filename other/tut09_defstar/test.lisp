(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

(ql:quickload "defstar")

(defpackage alain
  (:use :cl :defstar)
  (:export main));defpackage

(in-package :alain)

(declaim (type (integer) *anint*))
(defparameter *anint* 2)

(defun* ( addtwo -> integer ) ((x integer))
  ( + *anint* x ))

(defun main ()
	(print (addtwo 3)));main

(in-package :cl)
(defun main ()
  (alain::main))

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
