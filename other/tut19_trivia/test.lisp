(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

(ql:quickload "trivia")
(use-package :trivia)

(defstruct person name age)
(defvar *p* (make-person :name "Alain" :age 30))

(defun main ()
  (match '(1 2 3)
         ((cons  x y)
          (print x)
          (print y)
          ))
  (match '(1 2 3)
         ((list x y z)
          (print x)
          (print y)
          (print z)
          ))
  (match #(1 2 3)
         ((vector x y z)
          (print x)
          (print y)
          (print z)
          ))
  (match  *p*
          ((person :name name  :age  age)
           (print name)
           (print age)
           ))


  )

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
