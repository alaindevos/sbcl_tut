(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

;a string is a vector
;a vector is a 1-dim array
;an array is a sequence

(defvar *myglobal* 1)

(defun mystring ()
  (let ((s1 nil)
        (s2 nil)
        (s3 nil))
    (setf s1 "Alain")
    (setf s2 "De Vos")
    (setf s3 (concatenate 'string s1 s2))
    (print s3)
    (print (search "De" s3))
    (setf s3 (subseq s3 6 9));6-7-8
    (setf s3 (string-trim '(#\space) s3))
    (setf s3 (string-upcase s3))
    (print (equal "123" "123"))
    (print (char s1 0))));defun

(defun printkv (k v)
  (format t "key: ~A value:~A ~%" k v));defun

(defun myhash ()
  (let ((h1 nil)
        (h2 nil)
        (a  nil)
        (b  nil))
    (setf h1 '(("Alain" 20) ("Eddy" 30)))
    (print (assoc "Eddy" h1 :test #'equal))
    (setf h2 (make-hash-table :test 'equal))
    (setf (gethash "Alain" h2) 20)
    (setf (gethash "Eddy"  h2) 30)
    (print (gethash "Eddy" h2))
    (multiple-value-setq (a b) (gethash "Alain" h2))
    (print a)
    (print b)
    (maphash #'printkv h2)
    (print (hash-table-count h2))
    (remhash "Alain" h2)
    (clrhash h2)));defun

(defun mytype ()
  (let ((ar nil)
        (s  nil)
        (ar2 nil)
        (v   nil))
    (print (typep '(1 2 3 ) 'list ))
    (print (typep 1 'number))
    (setf ar (make-array '(3)))
    (setf (aref ar 0) 7)
    (print (aref ar 0))
    (setf s (make-sequence 'vector 5 :initial-element 0 ))
    (setf ar2 (make-array '(2 3)))
    (setf v (vector 0 0 0 0))));defun

(defun myeval ()
  (let((x nil))
      (setf x '(+ 1 2 3) )
      (print (eval x))));defun

(defun testkey (&key a b)
  (format t "a=~A b=~A ~%" a b));defun

(defun mykey ()
  (testkey :a "Alain" :b "De Vos"));defun

(defun myfuncall ()
  (funcall 'print "Hallo")
  (funcall #'print "Hallo"))

(defun mylambda ()
  (let ((res nil)
        (f (lambda (x) (+ x 1) ) ))
    (setf res (funcall f 1))
    (print res)));defun

(defun mydolist ()
  (let ((lis '(1 2 3))
        (sum  0    ))
    (dolist (x lis sum)
      (setf sum (+ x sum))
     )));defun

(defun mydotimes ()
  (let ((sum  0 ))
    (dotimes (i 4 sum)
      (setf sum (+ i sum)))));defun

(defun mydo ()
   (do ( (sum 0)
         (i 0 (+ 1 i)))    ; variable | initial_value | update_expression
         ((> i 3) sum)     ; condition | return_value
       (setf sum (+ i sum))));defun

(defun myreadfile ()
  (let ((inputstream nil)
        (outputstream nil))
    (with-open-file
      (outputstream "input.txt" :direction :output :if-exists :supersede)
      (print "a1 a2" outputstream)
      (print "b1 b2" outputstream)
      (print "c1 c2" outputstream)
      );openfile
    (with-open-file
      (inputstream "input.txt" :direction :input)
        (dotimes (i 1000)
          (let ((x (read-line inputstream nil nil)))
            (if (null x)
                (return)
                (format t "~A ~%" x)))))));defun


(defun main()
    (print "Start")
    (mytype)
    (mystring)
    (myhash)
    (myeval)
    (mykey)
    (myfuncall)
    (mylambda)
    (print (mydolist))
    (print (mydotimes))
    (print (mydo))
    (myreadfile)
  );main

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
