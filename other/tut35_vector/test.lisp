(load "~/quicklisp/setup.lisp")
;(ql:quickload :cl-cffi-gtk)

(defparameter avec (make-array '(1 2 3 4)))

(defun main ()
    (format t "~a" "Hello world")
    (format t "~a" (aref avec 2))
    (setf (aref avec 2) 5)
    (format t "~a" (aref avec 2)))
    
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
