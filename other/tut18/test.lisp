(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

(defparameter  *lis* '(1 2 3) )
(defun hello (&key k)
  (print k)
  );defun

(defun main ()
  (hello :k "mytext")
  (push 0 *lis*)
  (print *lis*)
  (print  (pop *lis*) )
  (print *lis*)
  )

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
