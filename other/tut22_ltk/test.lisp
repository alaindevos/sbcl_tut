(load "~/quicklisp/setup.lisp")
(ql:quickload :ltk)
(in-package :ltk-user)

(defun main()
  (with-ltk ()
   (let ((b (make-instance 'button 
                           :master nil
                           :text "Press Me"
                           :command (lambda ()
                                      (format t "Hello World!~&")))))
     (pack b))))
(sb-ext:save-lisp-and-die "test.exe" :toplevel #'ltk-user::main :executable t)
