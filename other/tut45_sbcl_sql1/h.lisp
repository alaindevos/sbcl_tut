;select * from messages_freebsd_2023021 order by datetime desc
;"datetime","host","program","pid","facility","priority","message"
(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))
(ql:quickload :postmodern)
(use-package :postmodern)

(defun myquery ()
  (let ( (datetime)  (program)  (pid) (message)
        (fdatetime)  (fprogram) (fpid) (fmessage)
        (sum));let
    (doquery (:order-by (:select 'datetime 'program 'pid 'message :from 'messages_gentoo_20230926) (:desc 'datetime))
                     ( datetime  program  pid  message)
             (setf fdatetime (format nil "~12A" datetime  ))
             (setf  fprogram (format nil "~10A" program   ))
             (setf      fpid (format nil  "~5A" pid       ))
             (setf  fmessage (subseq (format nil "~60A" message   ) 0 40 ))
             (setf       sum (concatenate 'string fdatetime "|" fprogram "|" fpid "|" fmessage ))
             (print sum)
             )));defun

(defun main ()
  (connect-toplevel "syslogng" "x" "x" "127.0.0.1" :port 5432 )
  (myquery));main

(sb-ext:save-lisp-and-die "h.exe" :toplevel #'main :executable t)
