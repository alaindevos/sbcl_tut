(load "~/quicklisp/setup.lisp")
(declaim (optimize (speed 3) (safety 3)))

(ql:quickload "series")
(use-package :series)

(defun integers ()
  "Returns a 'series' of all of the natural numbers."
  (declare (optimizable-series-function))
  (scan-range :from 1))

(defun squares ()
  "Returns a 'series' of all of the square numbers."
  (declare (optimizable-series-function))
  (map-fn t
          (lambda (x) (* x x))
          (integers)))

(defun sum-squares (n)
  "Returns the sum of the first N square numbers."
  (collect-sum (subseries (squares) 0 n)))

(defun myseries ()
  (print (sum-squares 10)));defun

(defun main ()
  (myseries));defun

(sb-ext:save-lisp-and-die "test.exe" :toplevel #'main :executable t)
